

import java.io.*;
import java.util.ArrayList;

public class student{
	
   String name;
   int age;
   String major;
   double GPA;
	
   // This is the constructor of the class student
   public student(String name){
      this.name = name;
   }
   // Assign the age of the Employee  to the variable age.
   public void age(int stuAge){
      age =  stuAge;
   }
   /* Assign the designation to the variable designation.*/
   public void major(String stuMajor){
      major = stuMajor;
   }
   public void GPA(double stuGPA){
	      GPA = stuGPA;
   }
   
   public void printStudent(){
	      System.out.println("Name:"+ name );
	      System.out.println("Age:" + age );
	      System.out.println("Major:" + major);
	      System.out.println("GPA:" + GPA);
   }
   public static void main(String args[]){
		/* Create two objects using constructor */
		student stuOne= new student("James Smith");
		student stuTwo = new student("Mary Anne");

		// Invoking methods for each object created
		stuOne.age(26);
		stuOne.major("art");
		stuOne.GPA(3);
		
		stuTwo.age(23);
		stuTwo.major("history");
		stuTwo.GPA(3.2);
		stuTwo.printStudent();
		//array of student
		student[] students = new student[2];
		students[0]=stuOne;
		students[1]=stuTwo;
		for(student stu : students){
			stu.printStudent();
		}
	}	
	  
}