
public class reverseString {
	    public static void main(String[] args){
	        String reverseMe = "nightall";

	        StringBuilder sb = new StringBuilder(reverseMe);
	        sb.reverse();

	        String reversed = sb.toString();

	        System.out.println("Before: " + reverseMe);
	        System.out.println("After: " + reversed);
	    }
}

